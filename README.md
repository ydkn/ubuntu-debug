# Ubuntu Debug Docker Image

## Overview

This docker image is based on `ubuntu:rolling` and adds a few tools when debuging e.g. network issues.

## Included packages

- openssh-server
- locales-all
- iproute2
- iputils-ping
- net-tools
- bind9-dnsutils
- netcat-openbsd
- tcpdump
- curl
- wget
- vim
- acl
- wireguard
- bash-completion
- rsync
- screen
- tmux
- htop
- kubectl
