FROM ubuntu:rolling

# args
ARG VCS_REF
ARG BUILD_DATE

# labels
LABEL maintainer="Florian Schwab <me@ydkn.io>" \
  org.label-schema.schema-version="1.0" \
  org.label-schema.name="ydkn/ubuntu-debug" \
  org.label-schema.description="Ubuntu Debug Docker Image" \
  org.label-schema.version="0.1" \
  org.label-schema.url="https://hub.docker.com/r/ydkn/ubuntu-debug" \
  org.label-schema.vcs-url="https://gitlab.com/ydkn/ubuntu-debug" \
  org.label-schema.vcs-ref=$VCS_REF \
  org.label-schema.build-date=$BUILD_DATE

# install packages
RUN DEBIAN_FRONTEND="noninteractive" apt-get update && apt-get install -y --no-install-recommends \
  apt-utils \
  ca-certificates \
  openssh-server \
  gnupg2 \
  locales-all \
  iproute2 \
  iputils-ping \
  net-tools \
  bind9-dnsutils \
  netcat-openbsd \
  tcpdump \
  curl \
  wget \
  vim \
  attr \
  acl \
  wireguard \
  bash-completion \
  rsync \
  screen \
  tmux \
  htop \
  && rm -rf /var/lib/apt/lists/*

# install kubectl
RUN k8s_version="$(curl -sL https://dl.k8s.io/release/stable.txt)" && \
  arch="$(dpkg --print-architecture)" && \
  curl -sL "https://dl.k8s.io/release/${k8s_version}/bin/linux/${arch}/kubectl" -o /usr/local/bin/kubectl && \
  chmod +x /usr/local/bin/kubectl && \
  echo "source <(kubectl completion bash); alias k=kubectl; complete -F __start_kubectl k" >>/etc/bash.bashrc
